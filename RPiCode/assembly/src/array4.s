
.data
.align 4
a:
	.word 10,20,30,40

.align 4
b:
	.space 16


.text
.global main
main:
	ldr r1, =a	@ base adress of a in r1
	ldr r4, =b	@ base adress of b in r4
	mov r2, #0

loop:
	cmp r2, #4
	beq end
	ldr r3, [r1], #+4	@ load value pointed to by r1 into r3 and then (post)increment r1 by 4
	str r3, [r4], #+4
	add r2, r2, #1
	b   loop	

end:
	/* call the print function */
	push {r4, lr}
	
	ldr r0, addr_of_array
	mov r1, #4
	bl print_array

	mov r0, #5
	pop {r4, lr}
	bx  lr
	
addr_of_array: .word a	

