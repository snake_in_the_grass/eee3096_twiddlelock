
.data
a:
	.word 10,20,30,40

b:
	.space 16

.text
.global main
main:
	ldr r1, =a	@ base adress of a in r1
	ldr r4, =b	@ base adress of b in r4
	mov r2, #0

loop:
	cmp r2, #3
	beq end
	ldr r3, [r1, +r2, LSL #2 ]
	str r3, [r4, +r2, LSL #2 ]
	add r2, r2, #1
	b   loop	

end:
	mov r0, #5
	bx  lr
	
	
