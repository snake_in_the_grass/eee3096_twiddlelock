.data

.balign 4
message1:
	.asciz "Hey there type in a number please: "

.balign 4
message2:
	.asciz "I read the number %d\n"

@ Format pattern for scanf
.balign 4
scan_pattern: 
	.asciz "%d"

@ This is where scanf will store its value that it reads
.balign 4
return:
	.word 0


.text
.global main
	ldr r1, = return
	str lr, [r1]		@ save return address

	ldr r0, =message
	bl  printf		@ call to printf

	ldr r0, = scan_pattern
	ldr r1, = number_read
	bl  scanf		@ call to scanf

	ldr r0, = message2
	ldr r1, = number_read
	ldr r1, [r1]
	bl  printf
