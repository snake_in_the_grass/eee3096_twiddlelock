.global main
.func main

main:
	mov r0, #21
	mov r1, #23
	cmp r0, r1
	beq case_equal
	
case_different:
	mov r0, #1
	b   end

case_equal:
	mov r0, #2
	b    end

end:
	bx  lr
