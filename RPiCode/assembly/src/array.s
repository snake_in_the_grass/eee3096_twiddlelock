/*
This program is an expreminet with arrays
*/

/* -- The data -- */
.data
a:
	.skip 400	@ Create a space of 400 bytes for the array (100 integers)

/* -- The program -- */
.text
.global main
main:
	ldr r1, =a	@ Load the address of a (&a) into r1
	mov r2, #0

/* 	
	r3 is used as the index 
	r1 is the base address of the array
	r2 is used to increment r3. 
	To compute the offset from r1, let r3 = r1 + 4*r2
*/
loop:
	cmp r2, #100
	beq end
	add r3, r1, r2, LSL #2
	str r2, [r3]	@r3 currently holds an address therefore use its value in a store
	add r2, r2, #1
	b   loop

end:
	mov r0, #20
	bx  lr
