.data

greeting:
	.asciz "Hello world"

.balign 4
return:
	.word 0


.text
.global main
main:
	/* -- Save the lr -- */
	ldr r1, =return		@ Used to store the return address
	str lr, [r1]		@ Good practice to take the lr content and store them in memory to restore at the end

	/* -- Print the greeting, changes lr to adress of next instruction -- */
	ldr r0, =greeting
	bl  puts

	/* -- Restore the lr -- */
	ldr r1, =return
	ldr lr, [r1]
	bx  lr			@ return from main using the lr

.global puts
