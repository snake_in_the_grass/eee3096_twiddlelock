#!/usr/bin/python3

import RPi.GPIO as GPIO
import Adafruit_MCP3008 as Ada
import time
import math
import os
from datetime import datetime, timedelta


# ---- Global variables
secure_mode = True  # this is true or false for the two modes

# ---- ISR
def toggleMode(channel):
    print("Toggle mode")
    global secure_mode
    # toggle the secure and unsecure modes


# ---- Set up SPI
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
SCLK = 11
MOSI = 10
MISO = 9
SS = 8

GPIO.setup(SCLK, GPIO.OUT)
GPIO.setup(MOSI, GPIO.OUT)
GPIO.setup(MISO, GPIO.IN)
GPIO.setup(SS, GPIO.OUT)

adc = Ada.MCP3008(clk = SCLK, 
        cs = SS,
        mosi = MOSI,
        miso = MISO)

readings = [0]*8     # An 8 element array to hold the values for each channel on the mcp


# ---- Setup GPIO input and output
# Buttons
listen_btn = 23
toggle_btn = 24
GPIO.setup(listen_btn, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(toggle_btn, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.add_event_detect(toggle_btn, GPIO.FALLING, callback=toggleMode, bouncetime =200)

# Output LEDs
secure_mode_led = 16
lock_led = 20
unlock_led = 21
GPIO.setup(secure_mode_led, GPIO.OUT)
GPIO.setup(lock_led, GPIO.OUT)
GPIO.setup(unlock_led, GPIO.OUT)


# ---- Setup up input sensor variables
pot_pin = 0



# ---- Function to convert the potentiometer reading to Volts
def getVolts(potVal, places):
    v = (potVal/float(1023)) * 3.3
    v = round(v, places)
    return v


# ---- Main
try:
    while(GPIO.input(listen_btn) == GPIO.HIGH):
        print("waiting")
        _ = os.system('clear')

        
    while True:
        
        print("Listening")
        pot_val = adc.read_adc(pot_pin)
        volts = getVolts(pot_val,2)
        print("Volts {}".format(volts))
        _ = os.system('clear')

except KeyboardInterrupt:
    print("Exiting")
    GPIO.cleanup()



