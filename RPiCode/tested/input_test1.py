#!/usr/bin/python3

import RPi.GPIO as GPIO
import Adafruit_MCP3008 as Ada
import time
import math
import os
from datetime import datetime, timedelta
from twiddleLockClasses import Combination
from twiddleLockClasses import Symbol


# ---- Global variables
secure_mode = True  # this is true or false for the two modes
DELAY = 0.075

# ---- ISR
def toggleMode(channel):
    print("Toggle mode")
    global secure_mode
    # toggle the secure and unsecure modes

# ---- Function to convert the potentiometer reading to Volts
def getVolts(potVal, places):
    v = (potVal/float(1023)) * 3.3
    v = round(v, places)
    return v

# ---- Set up SPI
GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
SCLK = 11
MOSI = 10
MISO = 9
SS = 8

GPIO.setup(SCLK, GPIO.OUT)
GPIO.setup(MOSI, GPIO.OUT)
GPIO.setup(MISO, GPIO.IN)
GPIO.setup(SS, GPIO.OUT)

adc = Ada.MCP3008(clk = SCLK, 
        cs = SS,
        mosi = MOSI,
        miso = MISO)

readings = [0]*8     # An 8 element array to hold the values for each channel on the mcp


# ---- Setup GPIO input and output
# Buttons
listen_btn = 23
toggle_btn = 24
GPIO.setup(listen_btn, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.setup(toggle_btn, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.add_event_detect(toggle_btn, GPIO.FALLING, callback=toggleMode, bouncetime =200)

# Output LEDs
secure_mode_led = 16
lock_led = 20
unlock_led = 21
GPIO.setup(secure_mode_led, GPIO.OUT)
GPIO.setup(lock_led, GPIO.OUT)
GPIO.setup(unlock_led, GPIO.OUT)


# ---- Setup up input sensor variables
pot_pin = 0

# ---- Main
try:
    while(True):

        combo = Combination()

        while(GPIO.input(listen_btn) == GPIO.HIGH):
            print("-------- Waiting --------")
            _ = os.system('clear')
            
        # ---- Initiate a new combination
        pause_time = 0
        start_pause_time = time.time()

        value = adc.read_adc(pot_pin)
        prev_value = value
        
        print("-------- Listening --------")
        
        # ---- Capture a new combination
        while(pause_time < 2.0):

            # ---- check equals with buffer
            if( abs(value - prev_value) < 3):
                prev_value = value
                value = adc.read_adc(pot_pin)
                pause_time = time.time() - start_pause_time
                
                time.sleep(DELAY)
                
            # ---- Turning right
            elif( value > prev_value ):
                print("RIGHT")
                start_right = time.time()   # start the right timer

                while( value > prev_value ):
                    print("Still RIGHT, value {}".format(value))
                    prev_value = value
                    value = adc.read_adc(pot_pin)

                    time.sleep(DELAY)
                
                start_pause_time = time.time()
                t_right = time.time() - start_right
                combo.addSymbol( Symbol('R', t_right))

            # ---- Turning left
            elif( value < prev_value ):
                print("LEFT")
                start_left = time.time()    # start the left timer

                while( value < prev_value  ):
                    print("Still LEFT, value {}".format(value))
                    prev_value = value
                    value = adc.read_adc(pot_pin)

                    time.sleep(DELAY)
                
                start_pause_time = time.time()
                t_left = time.time() - start_left
                combo.addSymbol( Symbol('L', t_left))
            
            else:
                print("Problem!")
        
        print("-------- End of combination capture --------")
        combo.print()
        time.sleep(DELAY*20)


                    

except KeyboardInterrupt:
    print("Exiting")
    GPIO.cleanup()



