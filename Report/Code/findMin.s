/* Function to find the minimum value of an array and return the memory address
   	Arguments:
	- r0 = &arr[], steps through the array
	- r1 = number of elements in the array

	Variables:
	- r2 = min
	- r3 = arr[i]
	- r4 = &min	
	Return:
	- r0 = address of the min element
*/

.data
return:	.word 0

.text
.global find_min


find_min:
	@push {r4, r5, r6, r7, r8, r9, r10, r11, ip, lr}
	
	ldr 	r2, =return
	str	lr, [r2]

	ldr 	r2, [r0]	@ min <- arr[0]
	mov 	r4, r0		@ r4 <- &arr[0]

loop:	
	cmp 	r1, #0		@ while r1>0
	beq end_1
	
	ldr	r3, [r0]	@ r3 <- arr[i]
	cmp	r3, r2		@ If (r3 < min) then change min
	blt new_min 
	sub r1, r1, #1		@ numElements --
	add r0, r0, #4		@ &arr[i] <- &arr[i+1]
	b loop

new_min:
	mov	r2, r3		@ min <- arr[i]
	mov	r4, r0		@ r4 <- &arr[i]
	sub 	r1, r1, #1	@ numElements --
	add	r0, r0, #4	@ &arr[i] <- &arr[i+1]
	b loop 
		
end_1:	
	mov r0, r4		@ output <- &arr[i] MIN ELEMENT 
@	pop {r4, r5, r6, r7, r8, r9, r10, r11, ip, lr}
	ldr	lr, =return
	ldr	lr, [lr]
	bx	lr
