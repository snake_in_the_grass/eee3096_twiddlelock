
.data
.align 4
integer_printf: .asciz "%d "
/* format string that simply prints a newline */
.align 4
newline_printf: .asciz "\n"



.text
.global print_array
print_array:

        /* r0 will be the address of the integer array */
    	/* r1 will be the number of items in the array */
    	push {r4, r5, r6, lr}  /* keep r4, r5, r6 and lr in the stack */
 
    	mov r4, r0             /* r4 ← r0. keep the address of the array */
    	mov r5, r1             /* r5 ← r1. keep the number of items */
    	mov r6, #0             /* r6 ← 0.  current item to print */
	
	b .Lprint_array_check_loop /* go to the condition check of the loop */
 
    	.Lprint_array_loop:
      		/* prepare the call to printf */
      		ldr r0, addr_of_integer_printf  
		/* r0 ← &integer_printf */
      		/* 
			load the item r6 in the array in address r4.
         		elements are of size 4 bytes so we need to 
			multiply r6 by 4 
		*/
      		ldr r1, [r4, +r6, LSL #2]       
		/* 
			r1 ← *(r4 + r6 << 2)
                   	this is the same as
                        r1 ← *(r4 + r6 * 4) 
		*/
      		bl printf       /* call printf */
 
      		add r6, r6, #1                  /* r6 ← r6 + 1 */

    	.Lprint_array_check_loop: 
      		cmp r6, r5               /* perform r6 - r5 and update cpsr */
      		bne .Lprint_array_loop   
		/* 
			if cpsr states that r6 is not equal to r5
                        branch to the body of the loop 
		*/
 
    	/* prepare call to printf */
    	ldr r0, addr_of_newline_printf /* r0 ← &newline_printf */
    	bl printf
 
    	pop {r4, r5, r6, lr}   /* restore r4, r5, r6 and lr from the stack */
    	bx lr                  /* return */
 
addr_of_integer_printf: .word integer_printf
addr_of_newline_printf: .word newline_printf

