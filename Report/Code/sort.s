.data
arr:	.word 69, 3, 40, 13, 24, 7, 102, 11, 2, 21, 1, 2, 3, 6, 5, 4

/*
r0 = output reg
r1 =
r2 = temp
r3 = temp 
r4 = 
r5 =
r6 =  
r7 = size of the unsorted array
r8 = the starting index of unsorted array
*/

.text
.global main

main:



/*Find the min, swap it and then move the starting point of the array one forward*/
	ldr r8, address_of_arr
	mov r7, #16

loop1:
	cmp r7, #1
	beq	print_me
	mov r1, r7
	mov r0, r8
	push { r2, r3, r4, r8, lr}
	bl 	find_min	
	pop { r2, r3, r4, r8, lr}		@ Returns the &min of the array into r0
	
	@Swap out the array elements
	b	swap
	

/* Move the mins, so temp <- arr[i], arr[i] <- min, min <- temp 
r0 = &min
r2 = temp 
r3 = temp
*/
swap:
	ldr r2, [r8]		@ r2 <- arr[i]
	ldr r3, [r0]		@ r3 <- min
	str r3, [r8]		@ min -> arr[i]
	str r2, [r0]		@ Swap min and arr[i]
	add r8, r8, #4		@ Move the starting pos of the array one forward
	sub r7, r7, #1		@ Decrease the size of the unsorted array
	b loop1

print_me:
	ldr r0, address_of_arr	@Starting ddress of the array	
	mov r1, #16		@Size of the array
	push {r4, lr}
	bl	print_array
	pop {r4, lr}
	
end:
	bx lr

address_of_arr: .word arr
