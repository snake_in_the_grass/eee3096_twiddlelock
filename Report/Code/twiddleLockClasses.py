#!/usr/bin/python3


''' -------- The symbol class -------- '''
class Symbol:

    tolerance = 0.25          # 250 ms tolerance for all instances of the Symbol class

    # Constructor/intitialiser
    def __init__(self, dir, t):
        self.dir = dir      # direction L/R
        self.t = t          # time in ms

    # Returns a string to print
    def toString(self):
        return "{} {:.2f}".format(self.dir, self.t)

    # Returns true if the symbols are within a tolerance of each other
    def isEqual(self, other):
        if(type(other) is Symbol):
            return (self.dir == other.dir) and ( abs(self.t - other.t) <= self.tolerance ) 
        else:
            print("Wrong type")
            return False

    def unsecureEqual(self, other):
        if(type(other) is Symbol):
            return ( ( abs(self.t - other.t) <= self.tolerance ) )
        else:
            print("Wrong type")
            return False


''' -------- The combination class -------- '''        
class Combination:

    # Attribute is a list of symbols
    def __init__(self):
        self.symbolList = []

    # Adds a symbol to the list if it is the correct type
    def addSymbol(self, s):
        if(type(s) is Symbol):
            self.symbolList.append(s)
        else:
            print("Wrong type - could not add symbol to combination")

    # SECURE Compares two combinations and returns True if equivalent. 
    def secureEqual(self, other):
        if(type(other) is not Combination):
            print("Not a combination - cannot compare")
            return False

        if(len(self.symbolList) != len(other.symbolList)):
            print("Combinations are different sizes")
            return False

        for i in range(0, len(self.symbolList)):
            if( not self.symbolList[i].isEqual( other.symbolList[i] )):
                return False
        
        # By this point they must be identical 
        return True

    
    # UNSECURE Compares two combinations and returns true if timings are equal
    def unsecureEqual(self, other):
        if(type(other) is not Combination):
            print("Not a combination - cannot compare")
            return False

        if(len(self.symbolList) != len(other.symbolList)):
            print("Combinations are different sizes")
            return False

        #Sort the two combinations wrt durations
        print("Sorting")
        self.symbolList.sort(key=lambda x: x.t)
        other.symbolList.sort(key=lambda x: x.t)

        for i in range(0, len(self.symbolList)):
            if( not self.symbolList[i].unsecureEqual( other.symbolList[i] )):
                return False
        
        # By this point they must be identical 
        return True


    # Prints the contents of the Combination object
    def print(self):
        for i in range(0, len(self.symbolList)):
            print(self.symbolList[i].toString() )
            



