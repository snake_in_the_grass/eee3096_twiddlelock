#!/usr/bin/python3

from twiddleLockClasses import Symbol
from twiddleLockClasses import Combination


''' -------- Testing symbols out --------'''
print()
print("---- Symbol Tests ----")
s1 = Symbol('L', 250)
s2 = Symbol('R', 200)

print(s1.toString() )
print(s2.toString() )
print("These symbols are equal? {}".format(s1.isEqual(s2)))


'''' -------- Testing combinations out -------- '''
print()
print("---- Combination Tests ----")
combo1 = Combination()

combo1.addSymbol(s1)
combo1.addSymbol(s2)
combo1.print()

combo2 = Combination()
combo2.addSymbol( Symbol('L', 240))
combo2.addSymbol( Symbol('R', 150))
combo2.print()

print("These combinations are equal? {}".format(combo1.isEqual(combo2)))
